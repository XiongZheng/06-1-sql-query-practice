/* 
 * 请告诉我 `employeeNumber` 最大的 employee 的如下信息：
 *
 * +─────────────────+────────────+───────────+
 * | employeeNumber  | firstName  | lastName  |
 * +─────────────────+────────────+───────────+
 */
select employeeNumber,firstName,lastName
from employees
where employeeNumber=(
select max (employeeNumber) from employees
);