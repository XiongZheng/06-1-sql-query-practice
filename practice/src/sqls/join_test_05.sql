/*
 * 请告诉我所有员工（employee）中有管理者（manager）的员工姓名及其管理者的姓名。所有的姓名
 * 请按照 `lastName, firstName` 的格式输出。例如：`Bow, Anthony`：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */
select concat(e1.lastName,', ',e1.firstName) as employee,
concat(e2.lastName,', ',e2.firstName) as manager
from employees e1
join employees e2
on e1.reportsTo is not null
and e1.reportsTo=e2.employeeNumber
order by manager,employee;