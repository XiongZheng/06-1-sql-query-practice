/*
 * 请告诉我所有的员工（employee）的姓名及其管理者的姓名。所有的姓名都需要按照 `lastName, firstName`
 * 的格式输出，例如 'Bow, Anthony'。如果员工没有管理者，则其管理者的姓名输出为 '(Top Manager)'。
 * 输出需要包含如下信息：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */
select distinct concat(e1.lastName,', ',e1.firstName) as employee,
case
when e1.reportsTo is null then '(Top Manager)'
else concat(e2.lastName,', ',e2.firstName)
end as manager
from employees e1
join employees e2
on e1.reportsTo is null
or e1.reportsTo=e2.employeeNumber
order by manager,employee;